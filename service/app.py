"""
Small application to simulate an auth mechanism and an api.
"""
import os
import jwt
import time
import uuid
import logging
from jwt.contrib.algorithms.pycrypto import RSAAlgorithm
from flask import Flask, request, jsonify
from flask_redis import FlaskRedis

try:
    jwt.register_algorithm("RS512", RSAAlgorithm(RSAAlgorithm.SHA512))
except ValueError as e:
    pass # Algorithm already has a handler

SECRET = "secret"
ALGORITHM = "HS256"
SERVICE_NAME = os.environ.get("SERVICE_NAME", "service")
SERVICE_PORT = os.environ.get("SERVICE_PORT", 5000)
REDIS_ADDR = os.environ.get("REDIS_MASTER_HOST", "localhost")
REDIS_PASSWD = os.environ.get("REDIS_MASTER_PASSWORD", "")
DEBUG = os.environ.get("DEBUG", False)

count_services_lua = """
local sum = 0
local matches = redis.call('KEYS', ARGV[1])
for _,key in ipairs(matches) do
    sum = sum + 1
end
return sum
"""

#log = logging.getLogger("werkzeug")
#log.setLevel(logging.ERROR)

app = Flask(__name__)
app.config["REDIS_URL"] = "redis://:%s@%s:6379/0" % (REDIS_PASSWD, REDIS_ADDR)
redis = FlaskRedis(app)

# register lua scripts
services_count = redis.register_script(count_services_lua)

@app.route("/")
def hello():
    redis.incr("hits")
    redis.incr(SERVICE_NAME)
    return jsonify({
        "service": SERVICE_NAME,
        "service_hits": int(redis.get(SERVICE_NAME)),
        "hits": int(redis.get("hits"))
    })

@app.route("/auth")
def auth():
    token = jwt.encode({"uuid": str(uuid.uuid1()), "ts": int(time.time())}, SECRET, algorithm=ALGORITHM).decode("utf-8")
    redis.set(token, "")
    return jsonify({
        "service": SERVICE_NAME,
        "token": str(token),
        "link": "http://localhost:8888/api?token=" + str(token),
        "running_services": services_count(args=["service:auth*"])
    })

@app.route("/api")
def api():
    if "Authorization" not in request.headers:
        return jsonify({
            "service": SERVICE_NAME,
            "error": "no token"
        })
    try:        
        token = request.headers["Authorization"]
        decoded = jwt.decode(token, SECRET, algorithm=["RS512"])    
        redis.incr("hits")
        redis.incr(SERVICE_NAME)
        return jsonify({
            "service": SERVICE_NAME,
            "service_hits": int(redis.get(SERVICE_NAME)),
            "hits": int(redis.get("hits")),
            "payload": decoded,
            "running_services": services_count(args=["service:service*"])
        })
    except Exception as e:
        app.logger.error(e)
        if "message" in e:
            err_msg = e.message
        else:
            err_msg = e        
        return jsonify({
            "error": err_msg
        })

if __name__ == "__main__":
    redis.set("service:" + SERVICE_NAME + ":" + SERVICE_PORT, str(int(time.time())))
    app.run(host="0.0.0.0", port=SERVICE_PORT, debug=DEBUG)    
