# openresty-lua-redis

This repositiory is an example, how to use OpenResty with Redis and Lua.  
It runs OpenResty, Redis with Backup, two service instances and a fake auth service.  

## Docker Compose

    $ docker-compose up --build

## Overview

![Overview](./overview.png?raw=true "Overview")

A request against `/` or `/auth` creates a token and set it on Redis, the response contains the token as part of the body.  
Additional to the token the response contains a link, name of the service and the count of running services.  

Requests against `/api` get handled with a Lua script to get the token as header or query param and set it as header.  

A Python script works as fake authentification and as service which replies and count the requests.  
The script contains a Flask application which runs three times, two times as api service and one time as auth service.  
Python also executes a Lua script to get the count of running services.  

## Test run

    $ pytest test_run.py

## Feedback
Star this repo if you found it useful. Use the github issue tracker to give feedback on this repo.