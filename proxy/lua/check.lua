local red = redis:new()
red:set_timeout(1000) -- 1 sec

--- Decode URL.
-- @param str string: URL.
-- @return string: decoded url
function url_decode(str)
    str = string.gsub (str, "+", " ")
    str = string.gsub (str, "%%(%x%x)",
        function(h) return string.char(tonumber(h,16)) end)
    str = string.gsub (str, "\r\n", "\n")
    return str
end

--- Get an access token from header or from the url query.
-- @return string: token
function getToken()
    local headers = ngx.req.get_headers()
    local auth_header = headers["Authorization"]    
    if auth_header then
        return auth_header
    else
        local url_param = ngx.var.arg_token;
        if url_param then
            local token = url_decode(ngx.var.arg_token);
            return token
        end
    end
    return
end

local token = getToken()

if token then
    -- connect to redis
    local ok, err = red:connect(os.getenv("REDIS_MASTER_HOST"), tonumber(os.getenv("REDIS_MASTER_PORT")))
    if not ok then
        local error_json = cjson.encode({
            service = "proxy",
            host = os.getenv("REDIS_MASTER_HOST"),
            port = os.getenv("REDIS_MASTER_PORT"),
            error = err
        })
        ngx.say(error_json)
        return
    end

    -- auth against redis
    local res, err = red:auth(os.getenv("REDIS_MASTER_PASSWORD"))
    if not res then
        local error_json = cjson.encode({
            service = "proxy",
            error = err
        })
        ngx.say(error_json)
    end
    
    -- is the token present in Redis
    local res, err = red:get(token)
    if res == ngx.null then
        ngx.say(cjson.encode({
            service = "proxy",
            error = "token is not present"
        }))
        ngx.exit(0)
    else
        ngx.var.token = token
    end    
else
    local error_json = cjson.encode({
        service = "proxy",
        error = "no token"
    })
    ngx.say(error_json)
    ngx.exit(0)
end
