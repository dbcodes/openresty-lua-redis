#!/usr/bin/env python
#−∗− coding: utf−8−∗−

import pytest
import requests

class TestRunClass:
    def test_status_call(self):
        status_reg = requests.get("http://127.0.0.1:8888/status")
        status_res = status_reg.json()
        assert "proxy" == status_res["service"]
    
    def test_error_call(self):
        auth_reg = requests.get("http://127.0.0.1:8888/auth")
        auth_res = auth_reg.json()
        err_reg = requests.get(auth_res["link"][:-10])
        err_res = err_reg.json()
        assert "token is not present" == err_res["error"]
    
    def test_right_call(self):
        auth_reg = requests.get("http://127.0.0.1:8888/auth")
        auth_res = auth_reg.json()
        reg = requests.get(auth_res["link"])
        res = reg.json()
        assert 1 <= res["hits"]
        assert 2 == res["running_services"]        
        assert 'service1' == res["service"] or 'service2' == res["service"]